Application.$controller("DetailsPageController", ["$scope", 'wmToaster', 'CONSTANTS', '$cordovaDialogs', 'Utils', function($scope, wmToaster, CONSTANTS, $cordovaDialogs, Utils) {
    "use strict";
    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };

    //handle mobile navbar back button click
    $scope.mobile_navbar1Backbtnclick = function($event, $isolateScope) {
        $scope.$emit('details-page:collapse');
    };

    //handle delete button
    $scope.deleteBookonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Book is deleted successfully.');
        $scope.$emit('details-page:collapse');
    };

    $scope.deleteBtnClick = function($event, $isolateScope) {
        if (CONSTANTS.hasCordova && Utils.isIphone()) {
            $cordovaDialogs.confirm('Would like to delete the book?')
                .then(function(buttonIndex) {
                    //OK
                    if (buttonIndex === 1) {
                        $scope.Variables.deleteBook.deleteRecord();
                    }
                });
        } else if (confirm('Would like to delete the book?')) {
            $scope.Variables.deleteBook.deleteRecord();
        }
    };


    $scope.editBtnClick = function($event, $isolateScope) {
        $scope.Variables.addBookPage.bookData = $scope.Variables.bookDetailsPage.dataSet.book;
    };



    $scope.LIBRARYUpdateCategoryDetailsonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Category is updated successfully.');
    };


    $scope.updateBookStatusonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Book status is updated successfully.');
    };


    $scope.updateBookRatingonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Book rating is updated successfully.');
    };

    $scope.addBtnClick = function($event, $isolateScope) {
        $scope.Variables.bookDetailsPage.dataSet.book.id = undefined;
        $scope.Variables.addBookPage.bookData = $scope.Variables.bookDetailsPage.dataSet.book;
    };

}]);