Application.$controller("MainPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };


    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */

    };


    $scope.bookPicClick = function($event, $isolateScope) {
        $scope.Variables.bookDetailsPage.dataSet.book = $isolateScope.$parent.item;
        $scope.Widgets.mobile_navbar1.show = false;
        $scope.Widgets.detailsView.show = true;
    };

    $scope.$on('details-page:collapse', function(event) {
        event.stopPropagation();
        $scope.Widgets.mobile_navbar1.show = true;
        $scope.Widgets.detailsView.show = false;
    });
}]);