Application.$controller("CategoryBookPageController", ["$scope", "wmToaster", 'CONSTANTS', '$cordovaDialogs', 'Utils', function($scope, wmToaster, CONSTANTS, $cordovaDialogs, Utils) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };

    $scope.bookPicClick = function($event, $isolateScope) {
        $scope.Variables.bookDetailsPage.dataSet.book = $isolateScope.$parent.item;
        $scope.Widgets.mobile_navbar1.show = false;
        $scope.Widgets.detailsView.show = true;
    };

    $scope.$on('details-page:collapse', function(event) {
        event.stopPropagation();
        $scope.Variables.getCategoryBooks.update();
        $scope.Widgets.mobile_navbar1.show = true;
        $scope.Widgets.detailsView.show = false;
    });

    $scope.anchor1Click = function($event, $isolateScope) {
        var msg = 'If this category is deleted, then all Books in this category will be moved to Miscellaneous Category. Would you like to delete this Category?';
        if (CONSTANTS.hasCordova && Utils.isIphone()) {
            $cordovaDialogs.confirm(msg)
                .then(function(buttonIndex) {
                    if (buttonIndex === 1) {
                        $scope.Variables.LIBRARYExecuteMoveBooksToMiscellaneousCategory.update();
                    }
                });
        } else if (confirm(msg)) {
            $scope.Variables.LIBRARYExecuteMoveBooksToMiscellaneousCategory.update();
        }
    };


    $scope.deleteCategoryonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Category is deleted successfully.');
    };

}]);