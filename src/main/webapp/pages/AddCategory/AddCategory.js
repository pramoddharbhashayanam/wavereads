Application.$controller("AddCategoryPageController", ["$scope", "$timeout", "wmToaster", function($scope, $timeout, wmToaster) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };

    $scope.categoryNameKeydown = function($event, $isolateScope) {
        if ($event.keyCode === 13 && $event.currentTarget.value.length > 0) {
            $isolateScope.datavalue = $event.currentTarget.value;
            $timeout(function() {
                $scope.Variables.addCategory.insertRecord();
            }, 100);
        }
    };


    $scope.anchor1Click = function($event, $isolateScope) {
        $scope.Variables.addCategory.insertRecord();
    };


    $scope.addCategoryonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Category is added successfully.');
        $scope.Variables.LIBRARYCategoryData.update();
        $scope.Variables.CATEGORY_LIST.update();
        $scope.Variables.TotalCategories.update();
    };

}]);