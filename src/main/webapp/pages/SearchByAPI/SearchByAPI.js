Application.$controller("SearchByAPIPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
        //$scope.Widgets.mobile_navbar1.showSearchBar();

    };
    $scope.bookPicClick = function($event, $isolateScope) {
        $scope.Variables.searchBookData.dataSet.book = $isolateScope.$parent.item;
        $scope.Variables.getTitle.setFilter('title', $scope.Variables.searchBookData.dataSet.book.volumeInfo.title);
        $scope.Variables.getTitle.setFilter('tenantid', $scope.Variables.tenantInfo.dataSet.data[0].tenantid);
        $scope.Variables.getTitle.update();
    }


    $scope.getTitleonSuccess = function(variable, data) {
        if (data.length) {
            $scope.Variables.bookDetailsPage.dataSet.book = data[0];
        } else {
            var bookDetailsPage = $scope.Variables.bookDetailsPage.dataSet.book;
            var searchBookData = $scope.Variables.searchBookData.dataSet.book.volumeInfo;
            bookDetailsPage.description = searchBookData.description;
            bookDetailsPage.isbn = searchBookData.industryIdentifiers[0].identifier;
            bookDetailsPage.publisher = searchBookData.publisher;
            bookDetailsPage.title = searchBookData.title;
            bookDetailsPage.author = searchBookData.authors && searchBookData.authors[0];
            bookDetailsPage.tenantid = $scope.Variables.tenantInfo.dataSet.data[0].tenantid;
            bookDetailsPage.readStatus = 1;
            bookDetailsPage.id = 0;
            bookDetailsPage.rating = 2;
            if (searchBookData.imageLinks && searchBookData.imageLinks.thumbnail) {
                bookDetailsPage.imageUrl = (searchBookData.imageLinks.thumbnail);
            } else {
                bookDetailsPage.imageUrl = $scope.Variables.defaultPicture.dataSet.image;
            }
        }
        $scope.Widgets.mobile_navbar2.show = false;
        $scope.Widgets.detailsView.show = true;
    };
    $scope.$on('details-page:collapse', function(event) {
        event.stopPropagation();
        $scope.Widgets.mobile_navbar2.show = true;
        $scope.Widgets.detailsView.show = false;
    });

}]);