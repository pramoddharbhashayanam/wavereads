Application.$controller("AddBookPageController", ["$scope", "$timeout", "wmToaster", function($scope, $timeout, wmToaster) {
    "use strict";

    var isBarcodeScan = false;

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        var description;
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
        if ($scope.Variables.addBookPage.bookData) {
            $scope.Variables.bookData.dataSet = $scope.Variables.addBookPage.bookData;
            $scope.Variables.addBookPage.bookData = {};
            description = $scope.Variables.bookData.dataSet.description;
            if (description && description.length > 250) {
                $scope.Variables.bookData.dataSet.description = description.substring(0, 250) + '...';
            }
            if (!$scope.Variables.bookData.dataSet.tenantid) {
                $scope.Variables.bookData.dataSet.tenantid = $scope.Variables.tenantInfo.dataSet.data[0].tenantid;
            }
            if ($scope.Variables.bookData.dataSet.imageUrl === null) {
                $scope.Variables.bookData.dataSet.imageUrl = 'resources/images/imagelists/Icon_default book3x.png';
            }
        }

    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
        $timeout(function() {
            //remove HTML input validation
            WM.element('.app-page:first form:first').attr('novalidate', true);
        }, 1500);
    };

    $scope.getISBNdataonSuccess = function(variable, data) {
        var bookData = $scope.Variables.bookData.dataSet,
            isbnData;
        bookData.readStatus = 1;
        bookData.rating = 3;

        if (data && data.totalItems > 0) {
            isbnData = data.items[0].volumeInfo;
            bookData.title = isbnData.title;
            bookData.author = isbnData.authors.join(',');
            bookData.publisher = isbnData.publisher;
            bookData.tenantid = $scope.Variables.tenantInfo.dataSet.data[0].tenantid;

            bookData.description = isbnData.description;
            if (isbnData.imageLinks) {
                bookData.imageUrl = (isbnData.imageLinks.thumbnail ? isbnData.imageLinks.thumbnail : isbnData.imageLinks.smallThumbnail);
            }
            $scope.Variables.goToManualSegment.navigate();
        } else if (isBarcodeScan) {
            isBarcodeScan = false;
            bookData.title = '';
            bookData.authors = '';
            bookData.publisher = '';
            bookData.description = '';
            wmToaster.show('info', 'INFO', 'Information is not available for this book.', 5000);
        }
    };


    $scope.uploadImageonSuccess = function(variable, data) {
        $scope.Variables.bookData.dataSet.imageUrl = data.path;
    };


    $scope.barcodescannerSuccess = function($event, $isolateScope) {
        if ($scope.Widgets.barcodescanner.datavalue && $scope.Widgets.barcodescanner.datavalue.length > 0) {
            isBarcodeScan = true;
            $scope.Variables.bookData.dataSet.isbn = $scope.Widgets.barcodescanner.datavalue;
            $scope.Variables.LIBRARYExecuteCheckIsbn.setInput('ISBN', $scope.Widgets.barcodescanner.datavalue);
            $scope.Variables.LIBRARYExecuteCheckIsbn.setInput('tenantid', $scope.Variables.tenantInfo.dataSet.data[0].tenantid);
            $scope.Variables.LIBRARYExecuteCheckIsbn.update();
            $timeout(function() {
                $scope.Variables.getISBNdata.update();
            }, 100);
        }
    };

    $scope.addBookFormBeforeservicecall = function($event, $operation, $data) {
        var formScope = WM.element('.add-book-page form:first').isolateScope();
        $scope.Variables.LIBRARYExecuteCheckIsbn.setInput('ISBN', $data.isbn);
        $data.id = $scope.Variables.bookData.dataSet.id;
        $data.imageUrl = $scope.Variables.bookData.dataSet.imageUrl;
        $data.tenantid = $scope.Variables.tenantInfo.dataSet.data[0].tenantid;

        if ($data.imageUrl === null) {
            $data.imageUrl = 'resources/images/imagelists/Icon_default book3x.png';
        }



        $data.category = _.findLast($scope.Variables.LIBRARYCategoryData.dataSet.data, function(c) {
            return c.cid == $data.category;
        });
        if ($data.title === null || _.trim($data.title).length === 0) {
            wmToaster.show('error', 'ERROR', 'Please add book title.', 5000);
            return false;
        }

        if ($data.description && $data.description.length > 250) {
            $data.description = $data.description.substring(0, 250) + '...';
        }

        if ($scope.Variables.bookData.dataSet.id > 0) {
            formScope.edit();
        } else {
            formScope.new();
        }
        if ($scope.Variables.LIBRARYExecuteCheckIsbn.dataSet.content.length > 0) {
            wmToaster.show('error', 'ERROR', 'Book with this ISBN already exists.', 5000);
            return false;
        }
    };


    $scope.getISBNdataonError = function(variable, data) {
        wmToaster.show('error', 'ERROR', 'Information is not available for this book.', 5000);
    };


    $scope.addBookFormSuccess1 = function($event, $operation, $data) {
        $scope.Variables.bookDetailsPage.dataSet.book = $scope.Variables.bookData.dataSet;
        $scope.Variables.goToLastPage.invoke();
    };

}]);


Application.$controller("addBookFormController", ["$scope", "$timeout",
    function($scope, $timeout) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.isbnChange = function($event, $isolateScope) {
            $scope.Variables.bookData.dataSet.isbn = $isolateScope._model_;
            $timeout(function() {
                $scope.Variables.getISBNdata.update();
                $scope.Variables.LIBRARYExecuteCheckIsbn.setInput('ISBN', $isolateScope._model_);
                $scope.Variables.LIBRARYExecuteCheckIsbn.setInput('tenantid', $scope.Variables.tenantInfo.dataSet.data[0].tenantid);
                $scope.Variables.LIBRARYExecuteCheckIsbn.update();
            }, 100);
        };
    }
]);