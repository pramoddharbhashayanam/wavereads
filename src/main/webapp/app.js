Application.run(['$rootScope', '$timeout', 'CONSTANTS', 'Utils', function($rootScope, $timeout, CONSTANTS, Utils) {
    "use strict";
    $rootScope.APP_RUN_MODE = true;
    /* perform any action with the variables inside this block(on-page-load) */
    $rootScope.onAppVariablesReady = function() {
        /*
         * variables can be accessed through '$rootScope.Variables' property here
         * e.g. $rootScope.Variables.staticVariable1.getData()
         */
    };

    function generateRandomString(len) {
        var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';
        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    }

    $rootScope.tenantId = localStorage.getItem('WAVEREADS_TENANT') || generateRandomString(16);

    function loadVariables() {
        $rootScope.Variables.miscellaneousCategory.update();
        $rootScope.Variables.CATEGORY_LIST.update();
        $rootScope.Variables.LIBRARYCategoryData.update();
        $rootScope.Variables.allBooks.update();
        $rootScope.Variables.readingBooks.update();
        $rootScope.Variables.mywishlist.update();
        $rootScope.Variables.readBooks.update();
        $rootScope.Variables.LIBRARYExecuteGetCategoryList.update();
    }

    $rootScope.tenantInfoonSuccess = function(variable, data) {
        if (data.length === 0) {
            //This device is not added
            $rootScope.Variables.addTenantDevice.insertRecord();

        } else {
            localStorage.setItem('WAVEREADS_TENANT', data[0].tenantid);
            $timeout(loadVariables, 100);
        }
    };



    $rootScope.deviceInfoonSuccess = function(variable, data) {
        if (CONSTANTS.hasCordova && Utils.isIphone()) {
            /*
             * IPHONE UUID is not same across app upgrades or reinstalls.
             * So, a plugin is used in case of iphone to generate a UUID which might not
             * be the same UUID that IOS assigns to this app.
             * This logic will be removed when user signup is supported.
             */
            window.plugins.uniqueDeviceID.get(function(uuid) {
                $rootScope.Variables.deviceInfo.dataSet.deviceUUID = uuid;
                $timeout(function() {
                    $rootScope.Variables.tenantInfo.update();
                }, 1000);
            });
        } else {
            $rootScope.Variables.tenantInfo.setFilter('deviceuuid', $rootScope.Variables.deviceInfo.dataSet.deviceUUID);
            $rootScope.Variables.tenantInfo.update();
        }
    };

    $rootScope.CATEGORY_LISTonSuccess = function(variable, data) {
        if (data.length === 0) {
            $rootScope.Variables.AddDefaultCategoriesSampleJavaOperation.update();
        }
    };


}]);
Application.run(['$rootScope', 'NavigationService', 'Utils', function($rootScope, NavigationService, Utils) {
    //Fix Start for Issue: on Iphone, two taps are required to open the keypad.
    var touchEventCancel = function(e) {
        e.stopPropagation();
    };
    $rootScope.$on('page-ready', function() {
        WM.element('.app-header input[type="search"]').parent().children().each(function() {
            this.removeEventListener('touchend', touchEventCancel, true);
            this.addEventListener('touchend', touchEventCancel, true);
        });
    });
    //Fix End for Issue: on Iphone, two taps are required to open the keypad.

    if (Utils.isIphone()) {
        WM.element('body').addClass('iphone');
    }


    //TODO in Wavemaker : NavigationService Back navigation is not in sync with borwser back button.
    NavigationService.goToPrevious = function() {
        // Use timeout to resolve IE digest cycle issue on route change
        setTimeout(function() {
            window.history.go(-1);
        }, 0);
    };


}])