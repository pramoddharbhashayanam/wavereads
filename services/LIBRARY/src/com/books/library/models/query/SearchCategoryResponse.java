/*Generated by WaveMaker Studio*/
package com.books.library.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class SearchCategoryResponse implements Serializable {


    @JsonProperty("NAME")
    @ColumnAlias("NAME")
    private String name;

    @JsonProperty("IMAGE_URL")
    @ColumnAlias("IMAGE_URL")
    private String imageUrl;

    @JsonProperty("TENANTID")
    @ColumnAlias("TENANTID")
    private String tenantid;

    @JsonProperty("CID")
    @ColumnAlias("CID")
    private Integer cid;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTenantid() {
        return this.tenantid;
    }

    public void setTenantid(String tenantid) {
        this.tenantid = tenantid;
    }

    public Integer getCid() {
        return this.cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchCategoryResponse)) return false;
        final SearchCategoryResponse searchCategoryResponse = (SearchCategoryResponse) o;
        return Objects.equals(getName(), searchCategoryResponse.getName()) &&
                Objects.equals(getImageUrl(), searchCategoryResponse.getImageUrl()) &&
                Objects.equals(getTenantid(), searchCategoryResponse.getTenantid()) &&
                Objects.equals(getCid(), searchCategoryResponse.getCid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(),
                getImageUrl(),
                getTenantid(),
                getCid());
    }
}
